import os
from setuptools import setup, find_packages
import shutil


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


REQUIREMENTS = [
    "numpy",
    "matplotlib",
]

EXTRAS = {
    "dev": [
        "black",
        "pre-commit",
        "pytest",
        "pytest-cov",
        "coverage",
        "pycobertura",
        "reslate",
    ]
}

CLASSIFIERS = [
    "Development Status :: 3 - Alpha",
    "Topic :: Scientific/Engineering :: Physics",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
]

KEYWORDS = "physics optics interferometry"


def write_config_file():
    configpath = os.path.join(
        os.environ.get("APPDATA")
        or os.environ.get("XDG_CONFIG_HOME")
        or os.path.join(os.environ["HOME"], ".config"),
        "cavcalc",
    )

    if not os.path.isdir(configpath):
        os.makedirs(configpath, exist_ok=True)

    usr_ini_file = os.path.join(configpath, "cavcalc.ini")
    if not os.path.exists(usr_ini_file):
        shutil.copyfile(
            os.path.join(os.path.dirname(__file__), "cavcalc/cavcalc.ini"), usr_ini_file
        )


write_config_file()

setup(
    # Package descriptions
    name="cavcalc",
    description=("cavcalc is a program for computing optical cavity parameters."),
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/sjrowlinson/cavcalc",
    # Versioning
    use_scm_version={"write_to": "cavcalc/version.py"},
    # Legal stuff and contacts
    license="GPL",
    author="Samuel Rowlinson",
    author_email="sjr@star.sr.bham.ac.uk",
    project_urls={
        "Source": "https://gitlab.com/sjrowlinson/cavcalc",
        "Documentation": "https://cavcalc.readthedocs.io/en/stable/",
    },
    # Packages and extensions
    packages=find_packages(),
    package_data={"cavcalc": ["_default.mplstyle", "cavcalc.ini"]},
    include_package_data=True,
    # Requirements
    python_requires=">=3.6",
    install_requires=REQUIREMENTS,
    setup_requires=["setuptools_scm"],
    extras_requires=EXTRAS,
    # Other
    classifiers=CLASSIFIERS,
    keywords=KEYWORDS,
    # CLI
    entry_points={"console_scripts": ["cavcalc = cavcalc.__main__:main"]},
)
