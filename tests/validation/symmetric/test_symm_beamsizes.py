import cavcalc as cc
from cavcalc.units import SI_DISTANCE_MAP
import numpy as np
from numpy.testing import assert_allclose
import pytest


##### SYMMETRIC CAVITY BEAMSIZE AT MIRRORS #####


@pytest.mark.parametrize(
    "L, gs", [(1, 0.5), [4e3, -0.8], (10e3, -0.94), (2, 0),],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_beamsizes_length_vs_gs_scalar(L, gs, wl):
    """Test calculation of single w for symmetric cavity."""
    out = cc.calculate("w", L=L, gs=gs, wl=wl)
    w = out.get()

    # Default units should be in cm
    assert w.units == "cm"

    w_analytic = lambda L, gs: np.sqrt((L * wl / np.pi) * np.sqrt(1 / (1 - gs**2)))
    w_expect = w_analytic(L, gs) * 1e2 # Convert to cm

    assert w.v == pytest.approx(w_expect)


@pytest.mark.parametrize(
    "L, gs", [((1, "cm"), 0.5), [(4, "km"), -0.8], ((10, "mm"), -0.94),],
)
@pytest.mark.parametrize(
    "wl", [(1.064, "um"), (1550, "nm"),]
)
def test_symm_beamsizes_length_vs_gs_scalar_with_units(L, gs, wl):
    """Test calculation of single w for symmetric cavity, with explicit units
    given for args."""
    out = cc.calculate("w", L=L, gs=gs, wl=wl)
    w = out.get()

    # Default units should be in cm
    assert w.units == "cm"

    w_analytic = lambda L, wl, gs: np.sqrt((L * wl / np.pi) * np.sqrt(1 / (1 - gs**2)))

    Lval, Lu = L
    L = Lval * SI_DISTANCE_MAP[Lu]
    wlval, wlu = wl
    wl = wlval * SI_DISTANCE_MAP[wlu]

    w_expect = w_analytic(L, wl, gs) * 1e2 # Convert to cm

    assert w.v == pytest.approx(w_expect)


@pytest.mark.parametrize(
    "L, gs", [(1, np.linspace(-1, 1, 101)), [4e3, np.linspace(-0.99, -0.01, 51)],],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_beamsizes_length_vs_gs_range(L, gs, wl):
    """Test calculation of w against gs array for symmetric cavity."""
    out = cc.calculate("w", L=L, gs=gs, wl=wl)
    w = out.get()

    # Default units should be in cm
    assert w.units == "cm"

    w_analytic = lambda L, gs: np.sqrt((L * wl / np.pi) * np.sqrt(1 / (1 - gs**2)))
    w_expect = w_analytic(L, gs) * 1e2 # Convert to cm

    assert_allclose(w.v, w_expect)


@pytest.mark.parametrize(
    "L, gs", [(np.linspace(0, 1, 50), 0.5), [np.linspace(3.9e3, 4.1e3), -0.8],],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_beamsizes_length_range_vs_gs(L, gs, wl):
    """Test calculation of w against length array for symmetric cavity."""
    out = cc.calculate("w", L=L, gs=gs, wl=wl)
    w = out.get()

    # Default units should be in cm
    assert w.units == "cm"

    w_analytic = lambda L, gs: np.sqrt((L * wl / np.pi) * np.sqrt(1 / (1 - gs**2)))
    w_expect = w_analytic(L, gs) * 1e2 # Convert to cm

    assert_allclose(w.v, w_expect)


@pytest.mark.parametrize(
    "L, Rc", [(1, 2), [4e3, 2090], (10e3, 5070), (2, 5e3),],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_beamsizes_length_vs_rocs_scalar(L, Rc, wl):
    """Test calculation of single w against RoC of mirrors for symmetric cavity. This
    will test the chained function calling as cavcalc does not provide a direct function
    of w(Rc)."""
    out = cc.calculate("w", L=L, Rc=Rc, wl=wl)
    w = out.get()

    # Default units should be in cm
    assert w.units == "cm"

    w_analytic = lambda L, Rc: np.sqrt((wl / np.pi) * np.sqrt(Rc * L / (2 - L / Rc)))
    w_expect = w_analytic(L, Rc) * 1e2 # Convert to cm

    assert w.v == pytest.approx(w_expect)


@pytest.mark.parametrize(
    "L, gouy", [(1, 20), [4e3, 330], (10e3, 310), (2, 179),],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_beamsizes_length_vs_rtgouy_scalar(L, gouy, wl):
    """Test calculation of single w against round-trip Gouy phase for symmetric cavity. This
    will also test conversion of units as Gouy phases should be converted from degrees (default
    input units for this parameter) to radians for calculations."""
    out = cc.calculate("w", L=L, gouy=gouy, wl=wl)
    w = out.get()

    # Default units should be in cm
    assert w.units == "cm"

    w_analytic = lambda L, gouy: np.sqrt((L * wl / (np.pi * np.sin(np.radians(gouy) / 2))))
    w_expect = w_analytic(L, gouy) * 1e2 # Convert to cm

    assert w.v == pytest.approx(w_expect)


def test_w_for_gsingle_abs_unity():
    """Beam size at mirrors should evaluate to inf for gs = +/- 1."""
    out = cc.calculate("w", L=1, gs=1)
    w = out.get()

    assert w.v == pytest.approx(np.inf)

    out = cc.calculate("w", L=1, gs=-1)
    w = out.get()

    assert w.v == pytest.approx(np.inf)


##### SYMMETRIC CAVITY WAISTSIZE #####


@pytest.mark.parametrize(
    "L, gs", [(1, 0.5), [4e3, -0.8], (10e3, -0.94), (2, 0),],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_waistsizes_length_vs_gs_scalar(L, gs, wl):
    """Test calculation of single w0 for symmetric cavity."""
    out = cc.calculate("w0", L=L, gs=gs, wl=wl)
    w0 = out.get()

    # Default units should be in cm
    assert w0.units == "cm"

    w0_analytic = lambda L, gs: np.sqrt((L * wl / (2 * np.pi)) * np.sqrt((1 + gs) / (1 - gs)))
    w0_expect = w0_analytic(L, gs) * 1e2 # Convert to cm

    assert w0.v == pytest.approx(w0_expect)


@pytest.mark.parametrize(
    "L, gs", [(1, np.linspace(-1, 1, 101)), [4e3, np.linspace(-0.99, -0.01, 51)],],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_waistsizes_length_vs_gs_range(L, gs, wl):
    """Test calculation of w0 against gs array for symmetric cavity."""
    out = cc.calculate("w0", L=L, gs=gs, wl=wl)
    w0 = out.get()

    # Default units should be in cm
    assert w0.units == "cm"

    w0_analytic = lambda L, gs: np.sqrt((L * wl / (2 * np.pi)) * np.sqrt((1 + gs) / (1 - gs)))
    w0_expect = w0_analytic(L, gs) * 1e2 # Convert to cm

    assert_allclose(w0.v, w0_expect)


@pytest.mark.parametrize(
    "L, gs", [(np.linspace(0, 1, 50), 0.5), [np.linspace(3.9e3, 4.1e3), -0.8],],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_waistsizes_length_range_vs_gs(L, gs, wl):
    """Test calculation of w0 against length array for symmetric cavity."""
    out = cc.calculate("w0", L=L, gs=gs, wl=wl)
    w0 = out.get()

    # Default units should be in cm
    assert w0.units == "cm"

    w0_analytic = lambda L, gs: np.sqrt((L * wl / (2 * np.pi)) * np.sqrt((1 + gs) / (1 - gs)))
    w0_expect = w0_analytic(L, gs) * 1e2 # Convert to cm

    assert_allclose(w0.v, w0_expect)


@pytest.mark.parametrize(
    "L, Rc", [(1, 2), [4e3, 2090], (10e3, 5070), (2, 5e3),],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_waistsizes_length_vs_rocs_scalar(L, Rc, wl):
    """Test calculation of single w0 against RoC of mirrors for symmetric cavity. This
    will test the chained function calling as cavcalc does not provide a direct function
    of w0(Rc)."""
    out = cc.calculate("w0", L=L, Rc=Rc, wl=wl)
    w0 = out.get()

    # Default units should be in cm
    assert w0.units == "cm"

    w0_analytic = lambda L, Rc: np.sqrt(
        (L * wl / (2 * np.pi)) * np.sqrt(2 * Rc / L - 1)
    )
    w0_expect = w0_analytic(L, Rc) * 1e2 # Convert to cm

    assert w0.v == pytest.approx(w0_expect)


@pytest.mark.parametrize(
    "L, gouy", [(1, 20), [4e3, 330], (10e3, 310), (2, 179),],
)
@pytest.mark.parametrize(
    "wl", [1064e-9, 1550e-9, 2090e-9,]
)
def test_symm_beamsizes_length_vs_rtgouy_scalar(L, gouy, wl):
    """Test calculation of single w0 against round-trip Gouy phase for symmetric cavity. This
    will also test conversion of units as Gouy phases should be converted from degrees (default
    input units for this parameter) to radians for calculations."""
    out = cc.calculate("w0", L=L, gouy=gouy, wl=wl)
    w0 = out.get()

    # Default units should be in cm
    assert w0.units == "cm"

    w0_analytic = lambda L, gouy: np.sqrt(
        (L * wl / (2 * np.pi)) * np.sqrt(
            (1 + np.cos(np.radians(gouy) / 2)) / (1 - np.cos(np.radians(gouy) / 2))
        )
    )
    w0_expect = w0_analytic(L, gouy) * 1e2 # Convert to cm

    assert w0.v == pytest.approx(w0_expect)



def test_w0_for_gsingle_minus_unity():
    """Waist size should be zero for gs = -1 (concentric cavity)."""
    out = cc.calculate("w0", L=1, gs=-1)
    w0 = out.get()

    # Default units should be in cm
    assert w0.units == "cm"

    assert w0.v == 0.0

def test_w0_for_gsingle_plus_unity():
    """Waist size should evaluate to inf for gs = 1."""
    out = cc.calculate("w0", L=1, gs=1)
    w0 = out.get()

    assert w0.v == pytest.approx(np.inf)
