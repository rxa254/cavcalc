import cavcalc as cc
from cavcalc.constants import SPEED_OF_LIGHT
from cavcalc.units import SI_DISTANCE_MAP
import numpy as np
import pytest


@pytest.mark.parametrize(
    "L", [1, 4e3, 10e3, 1e-6, 13e5, 0]
)
def test_fsr(L):
    out = cc.calculate("FSR", L=L)

    FSR = out.get()

    if L == 0:
        fsr_analytic = np.inf
    else:
        fsr_analytic = SPEED_OF_LIGHT / (2 * L)

    assert FSR.units == "Hz"
    assert FSR.v == pytest.approx(fsr_analytic)


@pytest.mark.parametrize(
    "L", [(1, "m"), (4, "km"), (1, "um"), (23, "nm"), (1.12, "pm")]
)
def test_fsr_with_length_units(L):
    out = cc.calculate("FSR", L=L)

    FSR = out.get()

    Lval, Lu = L
    L = Lval * SI_DISTANCE_MAP[Lu]

    if L == 0:
        fsr_analytic = np.inf
    else:
        fsr_analytic = SPEED_OF_LIGHT / (2 * L)

    assert FSR.units == "Hz"
    assert FSR.v == pytest.approx(fsr_analytic)
