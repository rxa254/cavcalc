.. _using_cavcalc:

Using cavcalc
=============

Here you will find details on how to get started with `cavcalc`. Included are the (very
simple) installation instructions, command line interface (CLI) guide and some examples
of how to use `cavcalc` as a Python module via its API.

.. toctree::
    :maxdepth: 1

    installation
    command_line
    module
    gfactors
