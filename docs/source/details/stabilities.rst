.. _stabilities:

Stability relations
===================

Symmetric cavities
------------------

.. rubric:: Stability of the mirrors...

As a function of the cavity length (:math:`L`) and mirror curvatures (:math:`R_c`),

.. math::
    g\left(L, R_c\right) = 1 - \frac{L}{R_c}.

As a function of the cavity length (:math:`L`) and radius of the beam at the mirrors (:math:`w`),

.. math::
    g\left(L, \lambda, w\right) = \pm \sqrt{1 - \frac{L\lambda}{\pi w^2}}.
