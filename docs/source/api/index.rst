API Documentation
=================

These pages contain references to the exposed classes, methods and functions of the cavcalc API. For
a more general guide on using cavcalc as a Python module, see :ref:`module`.

You shouldn't need to refer to any of this documentation if using cavcalc solely from the command
line, instead you should see :ref:`command_line` for details on this method of using the program.

.. autosummary::
    :toctree: .

    cavcalc
