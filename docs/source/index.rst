.. cavcalc documentation master file

.. only:: latex

    =======
    cavcalc
    =======

.. only:: not latex

    .. figure:: images/logo_cavcalc.png
        :align: center

The program `cavcalc` is a convenient tool for computing parameters associated
with Fabry-Perot style optical cavities. The recommended way to use
cavcalc is to invoke it via the command line, however a Python API
is also exposed such that it can be used in the same way as any other
Python module.

Installation is simple, just run::

    pip install cavcalc

to install the latest release version of cavcalc.

Project links:

- Python package index page: https://pypi.org/project/cavcalc/
- Follow the latest changes: https://gitlab.com/sjrowlinson/cavcalc

----

.. toctree::
    :maxdepth: 1
    :name: sec-usage

    using/index
    api/index
    details/index
    contributing/index
